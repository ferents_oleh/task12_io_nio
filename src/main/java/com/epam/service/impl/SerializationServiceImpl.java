package com.epam.service.impl;

import com.epam.service.SerializationService;

import java.io.*;

public class SerializationServiceImpl implements SerializationService {
    @Override
    public void writeObject(String path, Object object) {
        try (FileOutputStream fileOutputStream = new FileOutputStream(path);
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {

            objectOutputStream.writeObject(object);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Object readObject(String path) {
        Object object = null;
        try (FileInputStream fileInputStream = new FileInputStream(path);
             ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)) {

             object = objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return object;
    }
}
