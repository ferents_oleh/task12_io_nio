package com.epam.service.impl;

import com.epam.service.FolderService;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class FolderServiceImpl implements FolderService {
    @Override
    public List<File> getFolderContent(String path) {
        File f = new File(path);
        return Arrays.asList(f.listFiles());
    }
}
