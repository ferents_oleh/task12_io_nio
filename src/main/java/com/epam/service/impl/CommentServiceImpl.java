package com.epam.service.impl;

import com.epam.service.CommentService;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CommentServiceImpl implements CommentService {
    @Override
    public List<String> findCommentsFromFile(String filename) {
        List<String> comments = new ArrayList<>();
        String line;
        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader  reader = new BufferedReader(new FileReader(filename))) {
            line = reader.readLine();
            while(line != null) {
                line = line.trim();
                if (line.startsWith("//")) {
                    comments.add(line);
                } else if (line.startsWith("/*")) {
                    stringBuilder.append(line + "\n");
                    while (!line.startsWith(" */")) {
                        line = reader.readLine();
                        stringBuilder.append(line + "\n");
                    }
                    comments.add(stringBuilder.toString());
                }
                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return comments;
    }
}
