package com.epam.service;

import java.io.File;
import java.util.List;

public interface FolderService {
    List<File> getFolderContent(String path);
}
