package com.epam.service;

import java.util.List;

public interface CommentService {
    List<String> findCommentsFromFile(String filename);
}
