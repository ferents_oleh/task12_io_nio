package com.epam.service;

import java.io.FileNotFoundException;

public interface SerializationService {
    void writeObject(String path, Object object);

    Object readObject(String path);
}
