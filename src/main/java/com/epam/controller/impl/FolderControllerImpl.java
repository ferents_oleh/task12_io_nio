package com.epam.controller.impl;

import com.epam.controller.FolderController;
import com.epam.service.FolderService;

import java.io.File;
import java.util.List;
import java.util.Scanner;

public class FolderControllerImpl implements FolderController {
    private FolderService folderService;

    public FolderControllerImpl(FolderService folderService) {
        this.folderService = folderService;
    }

    @Override
    public void displayFolderContent() {
        Scanner scanner = new Scanner(System.in);
        String folderPath = scanner.next();
        printFolderInformation(folderService.getFolderContent(folderPath));
    }

    @Override
    public void cdFolder() {
        Scanner scanner = new Scanner(System.in);
        String folderPath = scanner.next();
        List<File> files = folderService.getFolderContent(folderPath);
        StringBuilder currentFolder = new StringBuilder(folderPath);
        String newFolder;
        printFolderInformation(files);
        do {
            newFolder = scanner.next();
            printFolderInformation(folderService.getFolderContent(currentFolder + newFolder));
            currentFolder.append(newFolder);
        } while (!newFolder.equals("q"));
    }

    private void printFolderInformation(List<File> files) {
        for (File file : files) {
            if (file.isFile()) {
                System.out.println("File " + file.getName());
                System.out.println("Readable = " + file.canRead());
                System.out.println("Executable = " + file.canExecute());
                System.out.println("Writable = " + file.canWrite());
            } else if (file.isDirectory()) {
                System.out.println("Folder " + file.getName());
            }
        }
    }
}
