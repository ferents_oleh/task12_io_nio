package com.epam.controller.impl;

import com.epam.controller.SerializationController;
import com.epam.model.Droid;
import com.epam.model.Ship;
import com.epam.service.SerializationService;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SerializationControllerImpl implements SerializationController {
    private SerializationService serializationService;

    public SerializationControllerImpl(SerializationService serializationService) {
        this.serializationService = serializationService;
    }

    @Override
    public void serializeObject() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter ship object: ");

        Ship ship = new Ship();

        String name = scanner.nextLine();
        ship.setName(name);
        int capacity = scanner.nextInt();
        ship.setCapacity(capacity);

        List<Droid> droids = new ArrayList<>();

        for (int i = 0; i < capacity; ++i) {
            System.out.println("Enter droid object: ");
            Droid droid = new Droid();
            String droidName = scanner.next();
            droid.setName(droidName);
            int droidDamage = scanner.nextInt();
            droid.setDamage(droidDamage);
            droids.add(droid);
        }
        ship.setDroids(droids);
        System.out.println("Enter file path:");
        String filepath = scanner.next();
        serializationService.writeObject(filepath, ship);
    }


    @Override
    public void deserializeObject() {
        Scanner scanner = new Scanner(System.in);
        String filepath = scanner.next();
        Object object = serializationService.readObject(filepath);
        System.out.println(object);
    }
}
