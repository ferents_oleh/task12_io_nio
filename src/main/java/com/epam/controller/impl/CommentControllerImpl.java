package com.epam.controller.impl;

import com.epam.controller.CommentController;
import com.epam.service.CommentService;

import java.util.List;
import java.util.Scanner;

public class CommentControllerImpl implements CommentController {
    private CommentService commentService;

    public CommentControllerImpl(CommentService commentService) {
        this.commentService = commentService;
    }

    @Override
    public void printComments() {
        Scanner scanner = new Scanner(System.in);
        String filename = scanner.next();
        List<String> comments = commentService.findCommentsFromFile(filename);
        for (String comment : comments) {
            System.out.println(comment);
        }
    }
}
