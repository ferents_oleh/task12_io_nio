package com.epam.controller;

public interface SerializationController {
    void serializeObject();

    void deserializeObject();
}
