package com.epam.view;


import com.epam.controller.CommentController;
import com.epam.controller.FolderController;
import com.epam.controller.SerializationController;

import java.util.*;

public class Menu {
    private String name;

    private String text;

    private LinkedHashMap<String, Runnable> actionsMap = new LinkedHashMap<>();

    private SerializationController serializationController;

    private CommentController commentController;

    private FolderController folderController;

    private Menu(final String name, final String text) {
        this.name = name;
        this.text = text;
    }

    public Menu(SerializationController serializationController,
                CommentController commentController,
                FolderController folderController) {
        this.serializationController = serializationController;
        this.commentController = commentController;
        this.folderController = folderController;

        Menu menu = new Menu("Main menu", "");
        menu.putAction("Serialize object", serializationController::serializeObject);
        menu.putAction("Deserialize object", serializationController::deserializeObject);
        menu.putAction("Print comments from source code file", commentController::printComments);
        menu.putAction("Print folder content", folderController::displayFolderContent);
        menu.putAction("cd command implementation", folderController::cdFolder);

        activateMenu(menu);
    }


    private void activateMenu(final Menu menu) {
        System.out.println(menu.generateMenu());
        Scanner scanner = new Scanner(System.in);
        while (true) {
            int actionNumber = scanner.nextInt();
            menu.executeAction(actionNumber);
        }
    }

    private void putAction(final String name, final Runnable action) {
        actionsMap.put(name, action);
    }

    private String generateMenu() {
        StringBuilder sb = new StringBuilder();
        sb.append(name).append(" ");
        sb.append(text).append(":\n");
        List<String> actionNames = new ArrayList<>(actionsMap.keySet());
        for (int i = 0; i < actionNames.size(); i++) {
            sb.append(String.format(" %d: %s%n", i + 1, actionNames.get(i)));
        }
        return sb.toString();
    }

    private void executeAction(int actionNumber) {
        actionNumber -= 1;
        if (actionNumber < 0 || actionNumber >= actionsMap.size()) {
            System.out.println("Wrong menu option: " + actionNumber);
        } else {
            List<Runnable> actions = new ArrayList<>(actionsMap.values());
            actions.get(actionNumber).run();
        }
    }
}
