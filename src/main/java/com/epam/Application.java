package com.epam;

import com.epam.controller.impl.CommentControllerImpl;
import com.epam.controller.impl.FolderControllerImpl;
import com.epam.controller.impl.SerializationControllerImpl;
import com.epam.service.impl.CommentServiceImpl;
import com.epam.service.impl.FolderServiceImpl;
import com.epam.service.impl.SerializationServiceImpl;
import com.epam.view.Menu;

public class Application {
    public static void main(String[] args) {
        new Menu(
                new SerializationControllerImpl(
                        new SerializationServiceImpl()
                ),
                new CommentControllerImpl(
                        new CommentServiceImpl()
                ),
                new FolderControllerImpl(
                        new FolderServiceImpl()
                )
        );
    }
}
