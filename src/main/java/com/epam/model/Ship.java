package com.epam.model;

import java.io.Serializable;
import java.util.List;

public class Ship implements Serializable {
    private String name;

    private transient int capacity;

    private List<Droid> droids;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public List<Droid> getDroids() {
        return droids;
    }

    public void setDroids(List<Droid> droids) {
        this.droids = droids;
    }

    @Override
    public String toString() {
        return "Ship{" +
                "name='" + name + '\'' +
                ", capacity=" + capacity +
                ", droids=" + droids +
                '}';
    }
}
