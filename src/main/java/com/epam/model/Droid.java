package com.epam.model;

import java.io.Serializable;

public class Droid implements Serializable {
    private String name;

    private int damage;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    @Override
    public String toString() {
        return "Droid{" +
                "name='" + name + '\'' +
                ", damage=" + damage +
                '}';
    }
}
